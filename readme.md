1. Dézipper le projet et l'ouvrir avec VSCode

2. Dans le terminal: 
composer install

3. Copier-coller le fichier .env existant, pour créer le fichier .env.local
Modifiez UNIQUEMENT le fichier .env.local !

4. Adaptez la ligne DATABASE_URL selon votre configuration. La base de données n'a pas besoin d'exister encore pour être renseignée ici ! Par exemple :
Avec mdp:
DATABASE_URL=mysql://root:root@127.0.0.1:8889/la_bonne_voiture?serverVersion=5.7
Sans mdp:
DATABASE_URL=mysql://root:@127.0.0.1:3306/la_bonne_voiture?serverVersion=5.7

5. Créer la db
Dans le terminal: 
php bin/console doc:database:create

6. Migrer la db
Dans le terminal: 
php bin/console doc:migrations:migrate

7. Charger les fixtures
Dans le terminal: 
php bin/console doc:fixtures:load --no-interaction

8. Lancer l'appli
Dans le terminal: 
symfony server:start
