<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200701113359 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE advert ADD user_reservation_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE advert ADD CONSTRAINT FK_54F1F40BD3B748BE FOREIGN KEY (user_reservation_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_54F1F40BD3B748BE ON advert (user_reservation_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE advert DROP FOREIGN KEY FK_54F1F40BD3B748BE');
        $this->addSql('DROP INDEX IDX_54F1F40BD3B748BE ON advert');
        $this->addSql('ALTER TABLE advert DROP user_reservation_id');
    }
}
