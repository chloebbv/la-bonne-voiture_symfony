<?php

namespace App\Controller;

use App\Entity\Advert;
use App\Form\AdvertType;
use App\Repository\AdvertRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


class AdvertController extends AbstractController
{
    /**
     * @Route("/", name="advert_index", methods={"GET"})
     */
    public function index(AdvertRepository $advertRepository): Response
    {
        return $this->render('advert/index.html.twig', [
            'adverts' => $advertRepository->findByIsNotReserved(),
            'h1Content' => 'Voici les annonces actuellement disponibles :'
        ]);
    }

    /**
     * @Route("/adverts", name="adverts", methods={"GET"})
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function findAll(AdvertRepository $advertRepository): Response
    {
        return $this->render('advert/index.html.twig', [
            'adverts' => $advertRepository->findAll(),
            'h1Content' => 'Toutes les annonces :'
        ]);
    }

    /**
     * @Route("/new", name="advert_new", methods={"GET","POST"})
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function new(Request $request): Response
    {
        $advert = new Advert();
        $form = $this->createForm(AdvertType::class, $advert);
        $form->add('save', SubmitType::class, ['label' => 'Ajouter']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $advert = $form->getData();
            $advert->setUser($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($advert);
            $entityManager->flush();

            return $this->redirectToRoute('advert_index');
        }

        return $this->render('advert/new.html.twig', [
            'advert' => $advert,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/show", name="advert_show", methods={"GET"})
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function show(Advert $advert): Response
    {
        return $this->render('advert/show.html.twig', [
            'advert' => $advert,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="advert_edit", methods={"GET","POST"})
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function edit(Request $request, Advert $advert): Response
    {
        if ($advert->getUser()->getId() == $this->getUser()->getID()) {
            $form = $this->createForm(AdvertType::class, $advert);
            $form->add('save', SubmitType::class, ['label' => 'Modifier']);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('advert_index');
            }

            return $this->render('advert/edit.html.twig', [
                'advert' => $advert,
                'form' => $form->createView(),
            ]);
        } else {
            return $this->redirectToRoute('advert_index');
        }
    }

    /**
     * @Route("/{id}", name="advert_delete", methods={"DELETE"})
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function delete(Request $request, Advert $advert): Response
    {
        if ($advert->getUser()->getId() == $this->getUser()->getID()) {
            if ($this->isCsrfTokenValid('delete' . $advert->getId(), $request->request->get('_token'))) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($advert);
                $entityManager->flush();
            }
        }

        return $this->redirectToRoute('advert_index');
    }

    /**
     * @Route("/my-published-adverts", name="my_published_adverts", methods={"GET"})
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function myAdverts(AdvertRepository $advertRepository): Response
    {
        return $this->render('advert/index.html.twig', [
            'adverts' => $advertRepository->findUserPublishedAdverts($this->getUser()->getId()),
            'h1Content' => 'Mes annonces :'
        ]);
    }

    /**
     * @Route("/{id}/book-reservation", name="advert_book_reservation", methods={"GET"})
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function book(Advert $advert): Response
    {
        if ($advert->getUser()->getId() != $this->getUser()->getID()) {
            $advert->setIsReserved(true);
            $advert->setUserReservation($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($advert);
            $entityManager->flush();
        }
        return $this->redirectToRoute('advert_index');
    }

    /**
     * @Route("/{id}/cancel-reservation", name="advert_cancel_reservation", methods={"GET"})
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function cancel(Advert $advert): Response
    {
        if ($advert->getUserReservation()->getId() == $this->getUser()->getID()) {
            $advert->setIsReserved(false);
            $advert->setUserReservation(null);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($advert);
            $entityManager->flush();
        }
        return $this->redirectToRoute('advert_index');
    }

    /**
     * @Route("/search", name="adverts_search", methods={"POST"})
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function search(Request $request, AdvertRepository $advertRepository): Response
    {
        $charSeq = $request->request->get('search');
        $charSeq = trim($charSeq);
        return $this->render('advert/index.html.twig', [
            'adverts' => $advertRepository->search($charSeq),
            'h1Content' => 'Résulats de la recherche "'. $charSeq . '" :'
        ]);
    }
}
