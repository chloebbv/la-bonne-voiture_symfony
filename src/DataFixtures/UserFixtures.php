<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < 2; $i++) {
            $userClient = new User();
            $userClient->setEmail('client' . $i . '@labonnevoiture.com');
            $userClient->setRoles(['ROLE_CLIENT']);
            $userClient->setPassword($this->encoder->encodePassword($userClient, 'lbvoiture'));
            $userClient->setFirstname($faker->firstName);
            $userClient->setLastname($faker->lastName);
            $manager->persist($userClient);
        }

        $manager->flush();
    }
}
