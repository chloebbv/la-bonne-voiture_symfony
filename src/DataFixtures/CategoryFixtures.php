<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $rent = new Category();
        $rent->setName("Location");
        $manager->persist($rent);

        $sale = new Category();
        $sale->setName("Vente");
        $manager->persist($sale);

        $manager->flush();
    }
}
