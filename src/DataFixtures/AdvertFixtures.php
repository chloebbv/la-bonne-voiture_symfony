<?php

namespace App\DataFixtures;

use App\Entity\Advert;
use App\Repository\BrandRepository;
use App\Repository\CategoryRepository;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Faker\Factory;

class AdvertFixtures extends Fixture implements DependentFixtureInterface
{
    private $brandRepository;
    private $categoryRepository;
    private $userRepository;

    public function __construct(BrandRepository $brandRepository, UserRepository $userRepository, CategoryRepository $categoryRepository)
    {
        $this->brandRepository = $brandRepository;
        $this->userRepository = $userRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        for ($i=0; $i < 8; $i++) {
            $advert = new Advert();
            $advert->setName('Modèle' . $i);
            $advert->setDescription($faker->text);
            $advert->setPrice($faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 50000));
            $advert->setUser($this->userRepository->find(rand(1,2)));
            $advert->setCategory($this->categoryRepository->find(rand(1,2)));
            $advert->setBrand($this->brandRepository->find(rand(1,5)));
            $manager->persist($advert);
        }

        for ($i=0; $i < 2; $i++) {
            $advert = new Advert();
            $advert->setName('Modèle' . $i);
            $advert->setDescription($faker->text);
            $advert->setPrice($faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 50000));
            $advert->setIsReserved(true);
            $advert->setUser($this->userRepository->find(rand(1,2)));
            do {
                $advert->setUserReservation($this->userRepository->find(rand(1,2)));
            } while ($advert->getUser() == $advert->getUserReservation());
            $advert->setCategory($this->categoryRepository->find(rand(1,2)));
            $advert->setBrand($this->brandRepository->find(rand(1,5)));
            $manager->persist($advert);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            BrandFixtures::class,
            CategoryFixtures::class,
            UserFixtures::class,
        );
    }
}
