<?php

namespace App\Repository;

use App\Entity\Advert;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Advert|null find($id, $lockMode = null, $lockVersion = null)
 * @method Advert|null findOneBy(array $criteria, array $orderBy = null)
 * @method Advert[]    findAll()
 * @method Advert[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdvertRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Advert::class);
    }

    /**
     * @return Advert[] Returns an array of Advert objects
     */

    public function findByIsNotReserved()
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.isReserved = 0')
            ->orderBy('a.date', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Advert[] Returns an array of Advert objects
     */

    public function findUserPublishedAdverts($value)
    {
        return $this->createQueryBuilder('a')
            ->join('a.user', 'user')
            ->andWhere('user.id = :val')
            ->setParameter('val', $value)
            ->orderBy('a.date', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function search($value)
    {
        return $this->createQueryBuilder('a')
            ->join('a.user', 'user')
            ->join('a.brand', 'brand')
            ->join('a.category', 'category')
            ->andWhere('user.firstname LIKE :val')
            ->orWhere('user.lastname LIKE :val')
            ->orWhere('category.name LIKE :val')
            ->orWhere('brand.name LIKE :val')
            ->orWhere('a.name LIKE :val')
            ->orWhere('a.description LIKE :val')
            ->orWhere('a.date LIKE :val')
            ->orWhere('a.price LIKE :val')
            ->setParameter('val', '%' . $value . '%')
            ->orderBy('a.date', 'DESC')
            ->getQuery()
            ->getResult();
    }
    

    /*
    public function findOneBySomeField($value): ?Advert
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
